/*
 *  RSS test app
 *  Copyright 2013 Denys Valchuk http://dvalchuk.com.ua.
 *  All Rights Reserved. Commercial in confidence.
 *  
 *  WARNING: This computer program is protected by copyright law and international
 *  treaties. Unauthorized use, reproduction or distribution of this program, or
 *  any portion of this program, may result in the imposition of civil and
 *  criminal penalties as provided by law.
 */
package ua.com.dvalchuk.rssapp;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import ua.com.dvalchuk.rss.resources.generated.Channels;
import ua.com.dvalchuk.rss.resources.generated.News;
import ua.com.dvalchuk.rss.resources.generated.ObjectFactory;
import ua.com.dvalchuk.rssapp.proc.JAXBHelper;

/**
 *
 * @author Denys Valchuk <dvalchuk@gmail.com>
 */
public class TopHandlerTest {
    private final Map<String,String> channels = new HashMap<String, String>(); 
    private final Random random = new Random();
   
    
    public TopHandlerTest() {
        channels.put("habra", "Habrahabr");
        channels.put("ibm", "IBM news");
        channels.put("msn", "MS news");
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    
    private InputStream getContentFrom(String path) throws IOException {
        HttpClient client = new DefaultHttpClient();
        HttpGet request = new HttpGet("http://localhost:8080" + path);
        HttpResponse response = client.execute(request);
        assertEquals(200, response.getStatusLine().getStatusCode());
        return response.getEntity().getContent();
    }
    
    private void postTo(String content, String path) throws IOException {
        HttpClient client = new DefaultHttpClient();
        HttpPost request = new HttpPost("http://localhost:8080" + path);
        StringEntity se = new StringEntity(content, "UTF8");
        se.setContentType("text/xml");
        request.setHeader("Content-Type", "text/xml;charset=UTF8");
        request.setEntity(se);
        HttpResponse response = client.execute(request);
        assertEquals(200, response.getStatusLine().getStatusCode());
    }
    
    private void putTo(String path) throws IOException {
        HttpClient client = new DefaultHttpClient();
        HttpPut request = new HttpPut("http://localhost:8080" + path);
        HttpResponse response = client.execute(request);
        assertEquals(200, response.getStatusLine().getStatusCode());
    }
    
    @Test
    public void testChannels() throws IOException {
        int channelsSz = 0;
        {
            InputStream is = getContentFrom("/rss-app/top/channels");
            final Channels jchs = JAXBHelper.fromInputStream(Channels.class, is);
            channelsSz = jchs.getChannel().size();
        }
        if (channelsSz < 1) {
            ObjectFactory of = new ObjectFactory();
            Channels jchs = of.createChannels();
            for(Map.Entry<String, String> e: channels.entrySet()) {
                Channels.Channel jch = of.createChannelsChannel();
                jch.setId(e.getKey());
                jch.setValue(e.getValue());
                jchs.getChannel().add(jch);
            }
            final String content = JAXBHelper.toString(Channels.class, jchs);
            postTo(content, "/rss-app/internal/channels");
        }
        {
            InputStream is = getContentFrom("/rss-app/top/channels");
            final Channels jchs = JAXBHelper.fromInputStream(Channels.class, is);
            final List<Channels.Channel> jchList = jchs.getChannel();
            assertFalse(jchList.isEmpty());
            assertEquals(3, jchList.size());
        }
    }
    
    private News generate100News(int rank) throws IOException {
        final List<String> chIds = new ArrayList<String> (channels.keySet());
        final ObjectFactory of = new ObjectFactory();
        News jnews = of.createNews();
        for(int i = 0; i < 100; ++i) {
            News.Item jit = of.createNewsItem();
            final int ri = random.nextInt(chIds.size());
            final String rchannelId = chIds.get(ri);
            jit.setChannel(rchannelId);
            final String link = "http://super.duper.host.ua/" + rchannelId + "/" + Integer.toString(i);
            jit.setLink(link);
            try {
                XMLGregorianCalendar xmlDate = DatatypeFactory.newInstance().newXMLGregorianCalendarDate(1000 + i, 7, 10, 120);
                jit.setDate(xmlDate);
            } catch (DatatypeConfigurationException ex) {
                throw new IOException(ex);
            }
            jit.setRank(rank);
            final String name = rchannelId + " News " + Long.toString(System.currentTimeMillis());
            jit.setValue(name);
            jnews.getItem().add(jit);
        }
        return jnews;
    }
    
    @Test
    public void testNews() throws IOException {
        putTo("/rss-app/internal/news/clear");
        {
            final News toSend = generate100News(1);
            final String content = JAXBHelper.toString(News.class, toSend);
            postTo(content, "/rss-app/internal/news");
        }
        {
            InputStream is = getContentFrom("/rss-app/top/news");
            final News jnews = JAXBHelper.fromInputStream(News.class, is);
            final List<News.Item> jits = jnews.getItem();
            assertFalse(jits.isEmpty());
            assertEquals(100, jits.size());
            final News.Item jit0 = jits.get(0);
            final News.Item jit1 = jits.get(1);
            final XMLGregorianCalendar cal0 = jit0.getDate();
            final XMLGregorianCalendar cal1 = jit1.getDate();
            int res = cal0.compare(cal1);
            assertEquals(DatatypeConstants.GREATER, res);
        }
        {
            final News toSend = generate100News(2);
            final String content = JAXBHelper.toString(News.class, toSend);
            postTo(content, "/rss-app/internal/news");
        }
        {
            InputStream is = getContentFrom("/rss-app/top/news");
            final News jnews = JAXBHelper.fromInputStream(News.class, is);
            final List<News.Item> jits = jnews.getItem();
            assertFalse(jits.isEmpty());
            assertEquals(100, jits.size());
            final News.Item jit0 = jits.get(0);
            final News.Item jit1 = jits.get(1);
            assertEquals(jit0.getRank(), jit1.getRank());
            assertEquals(2, jit1.getRank());
            final XMLGregorianCalendar cal0 = jit0.getDate();
            final XMLGregorianCalendar cal1 = jit1.getDate();
            int res = cal0.compare(cal1);
            assertEquals(DatatypeConstants.GREATER, res);
        }
        long lastIBMId = -1;
        int lastIBMRank = 0;
        {
            InputStream is = getContentFrom("/rss-app/top/news?channel=ibm");
            final News jnews = JAXBHelper.fromInputStream(News.class, is);
            final List<News.Item> jits = jnews.getItem();
            assertFalse(jits.isEmpty());
            for(News.Item jit: jits) {
                final String chanId = jit.getChannel();
                assertEquals("ibm", chanId);
                lastIBMId = jit.getId();
                lastIBMRank = jit.getRank();
            }
        }
        {
            final String path = "/rss-app/top/news/" + Long.toString(lastIBMId) + "?up";
            putTo(path);
            putTo(path);
            InputStream is = getContentFrom("/rss-app/top/news?channel=ibm");
            final News jnews = JAXBHelper.fromInputStream(News.class, is);
            final List<News.Item> jits = jnews.getItem();
            assertFalse(jits.isEmpty());
            News.Item jit0 = jits.get(0);
            assertEquals(lastIBMId, jit0.getId());
            assertEquals(lastIBMRank + 2, jit0.getRank());
        }
    }
    
}
