/*
 *  RSS test app
 *  Copyright 2013 Denys Valchuk http://dvalchuk.com.ua.
 *  All Rights Reserved. Commercial in confidence.
 *  
 *  WARNING: This computer program is protected by copyright law and international
 *  treaties. Unauthorized use, reproduction or distribution of this program, or
 *  any portion of this program, may result in the imposition of civil and
 *  criminal penalties as provided by law.
 */

package ua.com.dvalchuk.rssapp.proc;

import com.avaje.ebean.Ebean;
import java.io.IOException;
import java.util.List;
import javax.ws.rs.core.Response;
import ua.com.dvalchuk.rss.resources.News;

/**
 *
 * @author Denys Valchuk <dvalchuk@gmail.com>
 */
public class ClearNewsProcessor implements RequestProcessor {

    @Override
    public Response run() throws IOException {
        List<News> newss = News.all();
        Ebean.delete(newss);
        return Response.status(Response.Status.OK).entity("DONE").build();
    }

}
