/*
 *  RSS test app
 *  Copyright 2013 Denys Valchuk http://dvalchuk.com.ua.
 *  All Rights Reserved. Commercial in confidence.
 *  
 *  WARNING: This computer program is protected by copyright law and international
 *  treaties. Unauthorized use, reproduction or distribution of this program, or
 *  any portion of this program, may result in the imposition of civil and
 *  criminal penalties as provided by law.
 */

package ua.com.dvalchuk.rssapp.proc;

import com.avaje.ebean.Ebean;
import java.io.IOException;
import java.sql.Timestamp;
import javax.ws.rs.core.Response;
import javax.xml.datatype.XMLGregorianCalendar;
import ua.com.dvalchuk.rss.resources.Channel;
import ua.com.dvalchuk.rss.resources.generated.News;

/**
 *
 * @author Denys Valchuk <dvalchuk@gmail.com>
 */
public class AddNewsProcessor implements RequestProcessor {

    private final String content;

    public AddNewsProcessor(String content) {
        this.content = content;
    }

    @Override
    public Response run() throws IOException {
        final News jnews = JAXBHelper.fromString(News.class, content);
        for (News.Item jitem : jnews.getItem()) {
            ua.com.dvalchuk.rss.resources.News news = new ua.com.dvalchuk.rss.resources.News();
            final String jchannel = jitem.getChannel();
            Channel channel = Channel.findForId(jchannel);
            news.setChannel(channel);
            final Integer jrank = jitem.getRank();
            news.setRank(jrank);
            final String jlink = jitem.getLink();
            news.setLink(jlink);
            final XMLGregorianCalendar jdate = jitem.getDate();
            Timestamp timestamp = new Timestamp(jdate.toGregorianCalendar().getTimeInMillis());
            news.setCreated(timestamp);
            final String jtitle = jitem.getValue();
            news.setTitle(jtitle);
            Ebean.save(news);
        }
        return Response.status(Response.Status.OK).entity("DONE").build();
    }

}
