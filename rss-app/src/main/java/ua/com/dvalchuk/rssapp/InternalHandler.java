/*
 *  RSS test app
 *  Copyright 2013 Denys Valchuk http://dvalchuk.com.ua.
 *  All Rights Reserved. Commercial in confidence.
 *  
 *  WARNING: This computer program is protected by copyright law and international
 *  treaties. Unauthorized use, reproduction or distribution of this program, or
 *  any portion of this program, may result in the imposition of civil and
 *  criminal penalties as provided by law.
 */

package ua.com.dvalchuk.rssapp;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import org.apache.log4j.Logger;
import ua.com.dvalchuk.rssapp.proc.AddChannelsProcessor;
import ua.com.dvalchuk.rssapp.proc.AddNewsProcessor;
import ua.com.dvalchuk.rssapp.proc.ClearNewsProcessor;
import ua.com.dvalchuk.rssapp.proc.RequestProcessor;

/**
 * REST Web Service
 *
 * @author Denys Valchuk <dvalchuk@gmail.com>
 */
@Path("internal")
public class InternalHandler {
    static final Logger log = Logger.getLogger(InternalHandler.class);
    
    public InternalHandler() {
    }

    @POST
    @Path("channels")
    @Produces("text/plain; charset=utf8")
    @Consumes("text/xml; charset=utf8")
    public Response channels(String content) {
        try {
            RequestProcessor rp = new AddChannelsProcessor(content);
            return rp.run();
        } catch (Exception e) {
            log.error(e);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e.toString()).build();
        }
    }
    
    @POST
    @Path("news")
    @Produces("text/plain; charset=utf8")
    @Consumes("text/xml; charset=utf8")
    public Response news(String content) {
        try {
            RequestProcessor rp = new AddNewsProcessor(content);
            return rp.run();
        } catch (Exception e) {
            log.error(e);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e.toString()).build();
        }
    }
    
    @PUT
    @Path("news/clear")
    @Produces("text/plain; charset=utf8")
    public Response clearNews() {
        try {
            RequestProcessor rp = new ClearNewsProcessor();
            return rp.run();
        } catch (Exception e) {
            log.error(e);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e.toString()).build();
        }
    }
    
}
