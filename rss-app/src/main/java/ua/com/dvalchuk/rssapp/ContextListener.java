/*
 *  RSS test app
 *  Copyright 2013 Denys Valchuk http://dvalchuk.com.ua.
 *  All Rights Reserved. Commercial in confidence.
 *  
 *  WARNING: This computer program is protected by copyright law and international
 *  treaties. Unauthorized use, reproduction or distribution of this program, or
 *  any portion of this program, may result in the imposition of civil and
 *  criminal penalties as provided by law.
 */

package ua.com.dvalchuk.rssapp;

import com.avaje.ebeaninternal.server.lib.ShutdownManager;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 *
 * @author Denys Valchuk <dvalchuk@gmail.com>
 */
public class ContextListener implements ServletContextListener {
    private DBCleanDaemon dbcleaner;
    
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        dbcleaner = new DBCleanDaemon();
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        dbcleaner = null;
        ShutdownManager.shutdown();
    }
}
