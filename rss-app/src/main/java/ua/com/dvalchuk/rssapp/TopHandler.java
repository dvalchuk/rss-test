/*
 *  RSS test app
 *  Copyright 2013 Denys Valchuk http://dvalchuk.com.ua.
 *  All Rights Reserved. Commercial in confidence.
 *  
 *  WARNING: This computer program is protected by copyright law and international
 *  treaties. Unauthorized use, reproduction or distribution of this program, or
 *  any portion of this program, may result in the imposition of civil and
 *  criminal penalties as provided by law.
 */

package ua.com.dvalchuk.rssapp;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.apache.log4j.Logger;
import ua.com.dvalchuk.rssapp.proc.GetChannelsProcessor;
import ua.com.dvalchuk.rssapp.proc.GetNewsProcessor;
import ua.com.dvalchuk.rssapp.proc.RateNewsProcessor;
import ua.com.dvalchuk.rssapp.proc.RequestProcessor;



/**
 * REST Web Service
 *
 * @author Denys Valchuk <dvalchuk@gmail.com>
 */
@Path("top")
public class TopHandler {
    
    @Context
    private UriInfo context;
    static final Logger log = Logger.getLogger(TopHandler.class);

    public TopHandler() {
    }

    @GET
    @Path("channels")
    @Produces("text/xml; charset=utf8")
    public Response channels() {
        try {
            RequestProcessor rp = new GetChannelsProcessor();
            return rp.run();
        } catch (Exception e) {
            log.error(e);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e.toString()).build();
        }
    }
    
    @GET
    @Path("news")
    @Produces("text/xml; charset=utf8")
    public Response news(@QueryParam("channel") String channelId) {
        try {
            RequestProcessor rp = new GetNewsProcessor(channelId);
            return rp.run();
        } catch (Exception e) {
            log.error(e);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e.toString()).build();
        }
    }
    
    @PUT
    @Path("news/{id : \\d+}")
    @Produces("text/plain; charset=utf8")
    public Response newsRate(@PathParam("id") long newsId, @Context UriInfo info) {
        try {
            final MultivaluedMap mvm = info.getQueryParameters();
            final RateNewsProcessor.Direction direction = mvm.containsKey("up") ? RateNewsProcessor.Direction.up : RateNewsProcessor.Direction.down;
            RequestProcessor rp = new RateNewsProcessor(newsId, direction);
            return rp.run();
        } catch (Exception e) {
            log.error(e);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e.toString()).build();
        }
    }
    
}
