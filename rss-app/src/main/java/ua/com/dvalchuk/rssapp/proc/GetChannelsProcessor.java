/*
 *  RSS test app
 *  Copyright 2013 Denys Valchuk http://dvalchuk.com.ua.
 *  All Rights Reserved. Commercial in confidence.
 *  
 *  WARNING: This computer program is protected by copyright law and international
 *  treaties. Unauthorized use, reproduction or distribution of this program, or
 *  any portion of this program, may result in the imposition of civil and
 *  criminal penalties as provided by law.
 */

package ua.com.dvalchuk.rssapp.proc;

import java.io.IOException;
import java.util.List;
import javax.ws.rs.core.Response;
import ua.com.dvalchuk.rss.resources.Channel;
import ua.com.dvalchuk.rss.resources.generated.Channels;
import ua.com.dvalchuk.rss.resources.generated.ObjectFactory;

/**
 *
 * @author Denys Valchuk <dvalchuk@gmail.com>
 */
public class GetChannelsProcessor implements RequestProcessor {

    @Override
    public Response run() throws IOException {
        List<Channel> channels = Channel.all();
        ObjectFactory jfactory = new ObjectFactory();
        Channels jchannels = jfactory.createChannels();
        for (Channel ch : channels) {
            Channels.Channel jch = jfactory.createChannelsChannel();
            final String id = ch.getId();
            jch.setId(id);
            final String name = ch.getName();
            jch.setValue(name);
            jchannels.getChannel().add(jch);
        }
        final String result = JAXBHelper.toString(Channels.class, jchannels);
        return Response.status(Response.Status.OK).entity(result).build();
    }

}
