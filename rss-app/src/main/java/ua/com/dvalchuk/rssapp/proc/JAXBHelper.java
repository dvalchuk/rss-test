/*
 *  RSS test app
 *  Copyright 2013 Denys Valchuk http://dvalchuk.com.ua.
 *  All Rights Reserved. Commercial in confidence.
 *  
 *  WARNING: This computer program is protected by copyright law and international
 *  treaties. Unauthorized use, reproduction or distribution of this program, or
 *  any portion of this program, may result in the imposition of civil and
 *  criminal penalties as provided by law.
 */

package ua.com.dvalchuk.rssapp.proc;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.ValidationEvent;
import javax.xml.bind.util.ValidationEventCollector;
import javax.xml.namespace.QName;
import javax.xml.transform.stream.StreamSource;
import org.apache.log4j.Logger;
import ua.com.dvalchuk.rss.resources.generated.Channels;


/**
 *
 * @author Denys Valchuk <dvalchuk@gmail.com>
 */
public class JAXBHelper {
    private static final String DEFAULT_CHARSET = "UTF-8";
    static final Logger log = Logger.getLogger(JAXBHelper.class);
    
    public static <T> T read(Class<T> clazz, Reader reader) {
        try {
            JAXBContext context = JAXBContext.newInstance(clazz);
            Unmarshaller um = context.createUnmarshaller();
            ValidationEventCollector vec = new ValidationEventCollector();
            um.setEventHandler(vec);
            StreamSource ss = new StreamSource(reader);
            JAXBElement<T> root = um.unmarshal(ss, clazz);
            T obj = root.getValue();
            if (vec.hasEvents()) {
                final ValidationEvent[] valEvents = vec.getEvents();
                final StringBuilder msg = new StringBuilder("Events:");
                for (ValidationEvent ev : valEvents) {
                    final String msg1 = ev.getMessage();
                    msg.append("\n\t" + msg1);
                }
                throw new RuntimeException("XSD validation error during unmarshalling for class " + clazz.getName() + "\n" + msg.toString());
            }

            try {
                return clazz.cast(obj);
            } catch (ClassCastException exc) {
                throw new RuntimeException(
                        "Expected class " + clazz + " but was " + obj.getClass());
            }

        } catch (JAXBException e) {
            throw new RuntimeException(
                    "Error unmarshalling XML response", e);
        }
    }
    
    public static <T> void write(Writer writer, Class<T> clazz, T obj) {
        JAXBHelper.write(writer, clazz, obj, false);
    }

    public static <T> void write(Writer writer, Class<T> clazz, T obj, boolean formatted) {
        try {
            if (writer == null) {
                throw new RuntimeException("Writer object is null");
            }
            if (obj == null) {
                throw new RuntimeException("Object is null");
            }
            JAXBContext jc = JAXBContext.newInstance(obj.getClass());
            Marshaller ms = jc.createMarshaller();
            ms.setProperty(Marshaller.JAXB_ENCODING, JAXBHelper.DEFAULT_CHARSET);
            if (formatted) {
                ms.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            }
            ms.marshal(new JAXBElement<T>(new QName(clazz.getSimpleName()), clazz, obj), writer);
        } catch (JAXBException exc) {
            throw new RuntimeException("Failed to write " + obj.getClass().getName(), exc);
        }
    }
    
    public static <T> String toString(Class<T> clazz, T obj) throws IOException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try {
            Writer writer = new BufferedWriter(new OutputStreamWriter(out));
            JAXBHelper.write(writer, clazz, obj, true);
        } finally {
            out.close();
        }
        return new String(out.toByteArray(), JAXBHelper.DEFAULT_CHARSET);
    }
    
    public static <T> T fromInputStream(Class<T> clazz, InputStream is) throws IOException {
        UnicodeBOMInputStream ubis = new UnicodeBOMInputStream(is);
        ubis.skipBOM();
        Reader reader = new BufferedReader(new InputStreamReader(ubis, JAXBHelper.DEFAULT_CHARSET));
        return JAXBHelper.read(clazz, reader);
    }
    
    
    public static <T> T fromString(Class<T> clazz, String data) throws IOException {
        InputStream is = null;
        try {
            is = new ByteArrayInputStream(data.getBytes(JAXBHelper.DEFAULT_CHARSET));
            return JAXBHelper.fromInputStream(clazz, is);
        } finally {
            if (is != null) {
                is.close();
            }
        }
    }
    
}
