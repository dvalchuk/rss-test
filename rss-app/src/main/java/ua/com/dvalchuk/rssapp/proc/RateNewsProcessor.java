/*
 *  RSS test app
 *  Copyright 2013 Denys Valchuk http://dvalchuk.com.ua.
 *  All Rights Reserved. Commercial in confidence.
 *  
 *  WARNING: This computer program is protected by copyright law and international
 *  treaties. Unauthorized use, reproduction or distribution of this program, or
 *  any portion of this program, may result in the imposition of civil and
 *  criminal penalties as provided by law.
 */

package ua.com.dvalchuk.rssapp.proc;

import com.avaje.ebean.Ebean;
import java.io.IOException;
import javax.ws.rs.core.Response;
import ua.com.dvalchuk.rss.resources.News;

/**
 *
 * @author Denys Valchuk <dvalchuk@gmail.com>
 */
public class RateNewsProcessor implements RequestProcessor {
    private final long newsId;
    private final Direction direction;

    public static enum Direction {
        up("up"),
        down("down");
        private final String value;

        Direction(String v) {
            value = v;
        }

        public String value() {
            return value;
        }

        public static Direction fromValue(String v) {
            for (Direction e : Direction.values()) {
                if (e.value.equals(v)) {
                    return e;
                }
            }
            throw new IllegalArgumentException(v);
        }
    }

    public RateNewsProcessor(long newsId, Direction direction) {
        this.newsId = newsId;
        this.direction = direction;
    }
    
    @Override
    public Response run() throws IOException {
        News news = News.findForId(newsId);
        int rank = news.getRank().intValue();
        if (direction.equals(Direction.up)) {
            ++rank;
        } else {
            --rank;
        }
        news.setRank(rank);
        Ebean.save(news);
        return Response.status(Response.Status.OK).entity("DONE").build();
    }
    
}
