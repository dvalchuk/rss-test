/*
 *  RSS test app
 *  Copyright 2013 Denys Valchuk http://dvalchuk.com.ua.
 *  All Rights Reserved. Commercial in confidence.
 *  
 *  WARNING: This computer program is protected by copyright law and international
 *  treaties. Unauthorized use, reproduction or distribution of this program, or
 *  any portion of this program, may result in the imposition of civil and
 *  criminal penalties as provided by law.
 */

package ua.com.dvalchuk.rssapp.proc;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.ExpressionList;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.GregorianCalendar;
import java.util.List;
import javax.ws.rs.core.Response;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import ua.com.dvalchuk.rss.resources.generated.News;
import ua.com.dvalchuk.rss.resources.generated.ObjectFactory;

/**
 *
 * @author Denys Valchuk <dvalchuk@gmail.com>
 */
public class GetNewsProcessor implements RequestProcessor {
    private String channelId;

    public GetNewsProcessor(String channelId) {
        this.channelId = channelId;
    }

    @Override
    public Response run() throws IOException {
        ExpressionList<ua.com.dvalchuk.rss.resources.News> expressionList =
                Ebean.find(ua.com.dvalchuk.rss.resources.News.class).fetch("channel").where();
        if (channelId != null && !channelId.isEmpty()) {
            expressionList = expressionList.eq("channel_id", channelId); 
        }
        List<ua.com.dvalchuk.rss.resources.News> newss = expressionList
                .order().desc("rank")
                .order().desc("created")
                .setMaxRows(100)
                .findList();
        ObjectFactory jfactory = new ObjectFactory();
        News jnews = jfactory.createNews();
        for (ua.com.dvalchuk.rss.resources.News news : newss) {
            News.Item jit = jfactory.createNewsItem();
            final Long id = news.getId();
            jit.setId(id.longValue());
            final String channelId = news.getChannel().getId();
            jit.setChannel(channelId);
            final String link = news.getLink();
            jit.setLink(link);
            final Timestamp ts = news.getCreated();
            GregorianCalendar gc = new GregorianCalendar();
            gc.setTimeInMillis(ts.getTime());
            try {
                XMLGregorianCalendar xmlDate = DatatypeFactory.newInstance().newXMLGregorianCalendar(gc);
                jit.setDate(xmlDate);
            } catch (DatatypeConfigurationException ex) {
                throw new IOException(ex);
            }
            final Integer rank = news.getRank();
            jit.setRank(rank.intValue());
            final String name = news.getTitle();
            jit.setValue(name);
            jnews.getItem().add(jit);
        }
        final String result = JAXBHelper.toString(News.class, jnews);
        return Response.status(Response.Status.OK).entity(result).build();
    }

}
