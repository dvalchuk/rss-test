/*
 *  RSS test app
 *  Copyright 2013 Denys Valchuk http://dvalchuk.com.ua.
 *  All Rights Reserved. Commercial in confidence.
 *  
 *  WARNING: This computer program is protected by copyright law and international
 *  treaties. Unauthorized use, reproduction or distribution of this program, or
 *  any portion of this program, may result in the imposition of civil and
 *  criminal penalties as provided by law.
 */

package ua.com.dvalchuk.rssapp.proc;

import com.avaje.ebean.Ebean;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import javax.ws.rs.core.Response;

import ua.com.dvalchuk.rss.resources.Channel;
import ua.com.dvalchuk.rss.resources.generated.Channels;

/**
 *
 * @author Denys Valchuk <dvalchuk@gmail.com>
 */
public class AddChannelsProcessor implements RequestProcessor {
    private final String content;
    
    public AddChannelsProcessor(String content) {
        this.content = content;
    }

    @Override
    public Response run() throws IOException {
        final Channels jchs = JAXBHelper.fromString(Channels.class, content);
        for (Channels.Channel jch : jchs.getChannel()) {
            Channel ch = new Channel();
            final String jid = jch.getId();
            ch.setId(jid);
            final String jname = jch.getValue();
            ch.setName(jname);
            Ebean.save(ch);
        }
        return Response.status(Response.Status.OK).entity("DONE").build();
    }

}
