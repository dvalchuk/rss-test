/*
 *  RSS test app
 *  Copyright 2013 Denys Valchuk http://dvalchuk.com.ua.
 *  All Rights Reserved. Commercial in confidence.
 *  
 *  WARNING: This computer program is protected by copyright law and international
 *  treaties. Unauthorized use, reproduction or distribution of this program, or
 *  any portion of this program, may result in the imposition of civil and
 *  criminal penalties as provided by law.
 */

package ua.com.dvalchuk.rssapp;

import com.avaje.ebean.Ebean;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import ua.com.dvalchuk.rss.resources.News;
import ua.com.dvalchuk.rssapp.proc.ClearNewsProcessor;

/**
 *
 * @author Denys Valchuk <dvalchuk@gmail.com>
 */
public class DBCleanDaemon implements Runnable {
    private final Thread thread;
    private final long expireTime = 1000*60*15;
    
    public DBCleanDaemon() {
        thread = new Thread(this, "DBCleanDaemon");
        thread.start(); 
    }
    
    @Override
    public void run() {
        while (true) {
            try {
                Thread.sleep(expireTime);
            } catch (InterruptedException ex) {
            }
            List<News> newss = Ebean.find(News.class).fetch("channel").where()
                    .order().desc("rank")
                    .order().desc("created")
                    .setMaxRows(100)
                    .findList();
            List<News> backuped = new ArrayList<News>(newss.size());
            for (News news : newss) {
                News b = new News(news);
                backuped.add(b);
            }
            try {
                new ClearNewsProcessor().run();
            } catch (IOException ex) {
                throw new RuntimeException(ex);
            }
            Ebean.save(backuped);
        }
    }
    
}
