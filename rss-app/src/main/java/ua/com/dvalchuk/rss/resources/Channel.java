/*
 *  RSS test app
 *  Copyright 2013 Denys Valchuk http://dvalchuk.com.ua.
 *  All Rights Reserved. Commercial in confidence.
 *  
 *  WARNING: This computer program is protected by copyright law and international
 *  treaties. Unauthorized use, reproduction or distribution of this program, or
 *  any portion of this program, may result in the imposition of civil and
 *  criminal penalties as provided by law.
 */

package ua.com.dvalchuk.rss.resources;

import com.avaje.ebean.Ebean;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import java.util.List;
import com.avaje.ebean.validation.Length;



/**
 *
 * @author Denys Valchuk <dvalchuk@gmail.com>
 */
@Entity
@Table(name = "o_channel")
public class Channel implements Serializable {
    private static final long serialVersionUID = -6858157297100161134L;

    public static Channel findForId(String jchannel) {
        return Ebean.find(Channel.class, jchannel);
    }
    
    public static List<Channel> all() {
        return Ebean.find(Channel.class).findList();
    }
    
    @Id
    @Length(max = 16)
    String id;
    
    @Length(max = 100)
    String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
