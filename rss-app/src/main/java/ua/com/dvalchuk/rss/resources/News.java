/*
 *  RSS test app
 *  Copyright 2013 Denys Valchuk http://dvalchuk.com.ua.
 *  All Rights Reserved. Commercial in confidence.
 *  
 *  WARNING: This computer program is protected by copyright law and international
 *  treaties. Unauthorized use, reproduction or distribution of this program, or
 *  any portion of this program, may result in the imposition of civil and
 *  criminal penalties as provided by law.
 */

package ua.com.dvalchuk.rss.resources;

import com.avaje.ebean.Ebean;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author Denys Valchuk <dvalchuk@gmail.com>
 */
@Entity
@Table(name = "o_news")
public class News implements Serializable {
    private static final long serialVersionUID = 7775591810101447608L;

    @Id
    protected Long id;
    
    protected String link;
    
    protected String title;
    
    protected Integer rank;
    
    protected Timestamp created;
    
    @ManyToOne
    protected Channel channel;
    
    public News() {
    }
    
    public News(News another) {
        this.link = another.link;
        this.channel = another.channel;
        this.created = another.created;
        this.rank = another.rank;
        this.title = another.title;
    }
    
    public static News findForId(Long id) {
        return Ebean.find(News.class, id);
    }
    
    public static List<News> all() {
        return Ebean.find(News.class).findList();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getRank() {
        return rank;
    }

    public void setRank(Integer rank) {
        this.rank = rank;
    }

    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    public Channel getChannel() {
        return channel;
    }

    public void setChannel(Channel channel) {
        this.channel = channel;
    }
}
