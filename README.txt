﻿The goal is to create simple news RSS agregator. It should be implemented as REST web service and should has next API

Request sample: 
    GET /top/channels - returns list of channels registered in aggregator.

Response sample:

<channels>
   <channel id="habra">Habrahabr</channel>
   <channel id="ibm-dev">IBM developerWorks</channel>
</channels>

Request sample:
    GET /top/news?channel=habra - returns 100 news for specified channel sorted by rank and publication date.

Response sample:

<news>
    <item id="1" channel="habra" link="http://....." date="2013-01-31" rank="2">Develop Android Application</item>
    <item id="2" channel="habra" link="http://....." date="2013-01-30" rank="1">Google+</item>
</news>

Request sample:
    GET /top/news - returns 100 news for all channels sorted  by rank and publication date.

Response sample the same as for previous request.

Request smple:
    PUT  /top/news/1?up - increase the rank of the news on 1. <1> in the sample is news id.

Service should store news and channels in relational DB. News should be updated once per 15min. News which are not in top should be removed.

Service should be developed using JEE6.
For project description it is better to use Maven3.

